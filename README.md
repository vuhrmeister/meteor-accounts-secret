Meteor accounts-secret
======================

Adds possibility of logging in a user via token provided in URL.

Note: this is a very early version!    

**TODO:**
- tests
- Add default hook for iron router to automatically log in / out depending on presence of token in url
- some config options
