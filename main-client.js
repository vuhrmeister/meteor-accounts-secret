Meteor.loginWithSecret = function ( secretToken, callback ) {
  Accounts.callLoginMethod( {
    methodArguments: [ { secretToken: secretToken } ],
    userCallback: callback
  } );
};