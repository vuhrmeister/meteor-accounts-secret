Accounts.onCreateUser( function ( options, user ) {
  user.services.secret = {
    token: Random.id()
  };

  if ( options.profile ) {
    user.profile = options.profile;
  }

  return user;
} );

Accounts.registerLoginHandler( function ( loginRequest ) {
  if ( !loginRequest.secretToken ) {
    return;
  }

  var user = Meteor.users.findOne( {
    "services.secret.token": loginRequest.secretToken
  } );

  if ( !user ) {
    return;
  }

  return {
    userId: user._id
  };
} );