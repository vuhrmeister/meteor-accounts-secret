Package.describe( {
  name: "vuhrmeister:accounts-secret",
  version: "0.0.1",
  summary: "Token based authentication for Meteor",
  git: "https://github.com/vuhrmeister/meteor-accounts-secret.git",
  documentation: "README.md"
} );

Package.onUse( function ( api ) {
  api.use( "accounts-base@1.1.1" );

  api.addFiles( "main-client.js", "client" );
  api.addFiles( "main-server.js", "server" );
} );
